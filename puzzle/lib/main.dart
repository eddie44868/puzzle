import 'package:flutter/material.dart';
import 'package:puzzle/page/landing_page.dart';


void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter memory game',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
        buttonBarTheme: const ButtonBarThemeData(
          alignment: MainAxisAlignment.center
        )
      ),
      home: const LandingPage(),
      
    );
  }
}



