// ignore_for_file: deprecated_member_use, library_private_types_in_public_api
import 'dart:async';
import 'package:flutter/material.dart';
import '../model/board_item_model.dart';
import '../widgets/board_item.dart';

class LandingPage extends StatefulWidget {
  const LandingPage({Key? key}) : super(key: key);

  @override
  _LandingPageState createState() => _LandingPageState();
}

class _LandingPageState extends State<LandingPage> {
  int currentScore = 0;
  List<BoardItemModel> boardPuzzle = [];
  List<BoardItemModel> chosenItem = [];
  Timer? timeDelayed;
  List<String> apl = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P'];

  @override
  void initState() {
    setUpBoard();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Puzzle"),
        automaticallyImplyLeading: false,
      ),
      backgroundColor: const Color.fromARGB(115, 88, 81, 81),
      body: SingleChildScrollView(
        child: Container(
          padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 50),
          child: Column(children: [
            Container(
              alignment: Alignment.topLeft,
            ),
            GridView(
              shrinkWrap: true,
              physics: const ClampingScrollPhysics(),
              scrollDirection: Axis.vertical,
              gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
                  mainAxisSpacing: 0.0, maxCrossAxisExtent: 100.0),
              children: List.generate(boardPuzzle.length, (index) {
                return BoardItem(
                  itemModel: boardPuzzle[index],
                  onTap: () {
                    // onItemTap(index);
                  }, pic: apl[index],
                );
              }),
            ),
            const SizedBox(height: 20,),
            const Text('Silakan Menikmati Gamenya!', style: TextStyle(color: Colors.white),),
          ]),
        ),
      ),
    );
  }

//   void onItemTap(int index) {
//     var selectedItem = boardPuzzle[index];
//     if (!selectedItem.isCompleted) {
//       setState(() {
//         selectedItem.setRevealed(true);
//         closePreviousChosenItem();
//         chosenItem.add(selectedItem);
//         if (chosenItem.length == 1) {
//           if (chosenItem[0].imagePath == "assets/trophy.png") {
//             onItemCompleted();
//           } else {
//             if (timeDelayed != null) {
//               timeDelayed?.cancel();
//             }
//             timeDelayed = Timer(const Duration(seconds: 2), () {
//               closePreviousChosenItem();
//             });
//           }
//         }
//       });
//     }
//   }

//   void onItemCompleted() {
//     if (chosenItem.length == 1) {
//       setState(() {
//         chosenItem[0].setCompleted(true);
//         chosenItem.clear();
//         onCompleted();
//       });
//     }
//   }

//   void onCompleted() {
//     setState(() {
//       showDialog(
//         context: context,
//         builder: (context) => AlertDialog(
//           title: const Text('Anda Berhasil!', textAlign: TextAlign.center,),
//           content: const Text('Anda Telah memenangkan game ini!'),
//           actions: <Widget>[
//             FlatButton(
//               onPressed: () {
//                 Navigator.push(
//                   context,
//                   MaterialPageRoute(builder: (context) => const LandingPage()),
//                 );
//               },
//               child: const Text('Main Lagi'),
//             ),
//             FlatButton(
//                 onPressed: () {
//                   SystemNavigator.pop();
//                 },
//                 child: const Text("Keluar"))
//           ],
//         ),
//       );
//     });
//   }

//   void closePreviousChosenItem() {
//     if (chosenItem.length == 1) {
//       setState(() {
//         chosenItem[0].setRevealed(false);
//         chosenItem.clear();
//       });
//     }
//   }

  void setUpBoard() {
    List<BoardItemModel> allItem = [];
    for (int x = 0; x < 16; x++) {
        allItem.add(BoardItemModel(imagePath: "assets/box1.png"));
    }
    // for (int x = 0; x < 16; x++) {
    //   if (x == 12) {
    //     allItem.add(BoardItemModel(imagePath: 'assets/box1.png'));
    //   } else {
    //     allItem.add(BoardItemModel(imagePath: "assets/cross.png"));
    //   }
    // }

    boardPuzzle = [];
    var randomItem = (allItem..shuffle()).take(16);
    boardPuzzle.addAll(randomItem.map((item) => item.copy()).toList());
    setState(() {
      boardPuzzle.shuffle();
      currentScore = 0;
    });
  }

  void onPlay() {
    setUpBoard();
  }
}
