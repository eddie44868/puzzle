import 'package:flutter/material.dart';
import '../model/board_item_model.dart';

class BoardItem extends StatelessWidget {
  final BoardItemModel itemModel;
  final Function() onTap;
  final String pic;

  // ignore: prefer_const_constructors_in_immutables
  BoardItem({Key? key, required this.itemModel, required this.onTap, required this.pic}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        margin: const EdgeInsets.all(5),
        height: 40,
        width: 40,
        color: const Color.fromARGB(96, 60, 60, 60),
        child: Stack(
          children: [
            // Image.asset(itemModel.isRevealed || itemModel.isCompleted
            //     ? itemModel.imagePath
            //     : 'assets/box1.png',
            //     ),
            Center(child: Text(pic, style: const TextStyle(color: Colors.white),),)
          ],
        ),
      ),
    );
  }
}
